<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Utils\AppConst;
// use App\Models\User\User;

class Product extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'category_id',
        'image',
        'unit_price',
        'quantity',
        'size',
        'color',
        'weight',
        'is_trending',
        'description',
    ];

    public function categories()
    {
        return $this->belongsToMany(Category::class, 'categories_products','product_id', 'id');
    }


    public function orderDetails()
    {
        return $this->hasMany(OrderDetail::class);
    }

        // need to solve
    public function cartItem()
    {
        return $this->belongsTo(CartItem::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Undocumented function
     *
     * @param array $params
     * @return void
     */
    // public function fetchProducts($params=[]){
    //     $qry = $this->select('id','title', 'price');

    //     if(isset($params['searchKey'])){
    //         $searchKey = $params['searchKey'];
    //         $qry->where('title', 'LIKE', "%$searchKey%")
    //         ->orWhere('price', 'LIKE', "%$searchKey%")
    //         ->orWhere('description', 'LIKE', "%$searchKey%");
    //     }
    //     $limit = isset($params['limit']) ? $params['limit'] : AppConst::LIMIT;
    //     $offset = isset($params['offset']) ? $params['offset'] : AppConst::OFFSET;
    //     // $result = $qry->skip($offset)->take($limit)->get();
    //     $result = $qry->paginate($limit);
    //     return $result;
    // }
    public function fetchProducts()
    {
        $product = Product::where('user_id' , auth()->user()->id)->orderBy('id' , 'DESC')->paginate(16);
        return $product;
    }
    public function listOfProducts()
    {
        $product = Product::paginate(16);
        return $product;
    }

}
