<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CartItem extends Model
{
    use HasFactory;
        // problem need to solve
    // public function user()
    //  {
    //      return $this->belongsTo(User::class);
    //  }

        // need to solve
     public function product()
     {
         return $this->hasMany(Product::class);
     }

     public function cart(){
        return $this->belongsTo(Cart::class);
    }
}
