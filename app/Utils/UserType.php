<?php
namespace App\Utils;

class UserType{
    const ADMIN=1;
    const CUSTOMER=2;
    public $types=[

        self::ADMIN => "Admin",
        self::CUSTOMER => "Customer"
    ];
}