<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;



class AdminController extends Controller
{
   
    public function showUser()
    {
        $member = User::all();

       return view('Admin/view-users',['member'=>$member]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function userEdit($id)
    {
        $users=User::find($id);
       
        return view('admin.users-edit')->with('users',$users);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function userUpdate(Request $request)
    {
        $users=User::find($request->id);
        $users->name=$request->name;
        $users->email=$request->email;
        $users->password=$request->password;
        $users->save();
        return redirect('/users')->with('status','your Data is updateed');
     
    }

    public function deleteUser($id){
    $data=User::find($id);
    $data->delete();
 return redirect('/users')->with('status','your Data is Deleted');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   
}
