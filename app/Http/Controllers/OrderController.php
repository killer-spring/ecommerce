<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\Product;
use App\Models\Cart;
use App\Models\CartItem;
use App\Models\OrderDetail;
use Illuminate\Http\Request;

class OrderController extends Controller
{

    public function placeOrder(Request $request){
        $product = new Product;
        $order = new Order;
        $cart = new Cart;
        $cartItems = new CartItem;
        $order->quantity = 1;
        $order->price = $request->price;
        $order->restaurant = $request->restaurant_id;
        $order->customer_id = auth()->user()->id; // you can add this to tag the customer info
        $order->save();
        
        $cartItems = [];
        foreach ($cart->items as $ItemId => $item) {
            $cartItems[] = [
                'order_id' => $order->id,
                'product_id' => $product->id,
                'quantity' => $item['qty']
            ];
        }
    }

  

    // orderDetails

    public function viewOrderDetail($id)
    {
        $order = OrderDetail::find($id);

    	return redirect('');

    }



    public function fetchOrder(){
        $allOrder = Order::all();
        // dd($allOrder);

        return redirect('');

    }


    public function fetchOrderDetails(){
        $allOrder = OrderDetail::all();
        return redirect('');

    }

    public function viewOrder($id)
        {
            $order = Order::find($id);
            // dd($order);
            return redirect('');
        }

    public function deleteOrder($id)
    {
        $order =  Order::find($id);
        //@todo - remove dd and implement proper code
        dd($order->delete());
        return redirect('');
    }


    public function deleteOrderDetails($id)
    {
        $order =  OrderDetail::find($id);

        dd($order->delete());
        return redirect('');
    }

}
