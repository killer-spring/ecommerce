<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Product;

class CategoryController extends Controller
{
   
    public function fetchCategory(){
    $allCategory = Category::all();
    return view('admin.add-category',['allCategory'=>$allCategory]);
}
public function productCategory(){
    $maincategory = Category::all();
    /**
     * $product = Product::find($id);*/
    return redirect('admin.add-product', ['maincategory'=>$maincategory]);

}
public function  show($id) {
    $category = Category::find($id);
    return view('admin.edit-cat')->with('category',$category);
}
public function update(Request $request)
{
    $category = Category::find($request->id);
    $category->name=$request->name;
    $category->sub_cat=$request->sub_cat;
    $category->save();
    return redirect('/allcategory');
        

    }
     public function remove($id) 
     {
        $category = Category::find($id);
        $category->delete();
        return redirect('/allcategory');
        //dd($category);
    }
     public function getCategory($id) 
     {
        $category = Category::find($id);
        
        //dd($category);
    }
    public function addCategory(Request $request) {
      // dd('new catogry add option');
      $category = new Category();
      $category->name=$request->name;
      $category->sub_cat=$request->sub_cat;
      $category->save();
      return redirect('/category');
     }
     
}
