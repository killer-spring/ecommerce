<?php

namespace App\Http\Controllers;
use App\Models\User;

use Illuminate\Http\Request;

class UserController extends Controller
{
 /**
     * User LogIn .
     *
     * @return void
     */
    public function login(Request $req)
    {
    	$User = new User;
        $User->email=$req->email;
    	$User->password=$req->password;
    	$User->save();
    	return redirect('');

    }

     /**
     * Register User.
     *
     * @return void
     */

    public function saveUser(Request $req)
    {
        $User = new User;
        $User->name=$req->name;
        $User->email=$req->email;
        $User->phone=$req->phone;
        $User->gender=$req->gender;
        $User->city=$req->city;
        $User->address=$req->address;
        $User->date_of_birth=$req->date_of_birth;
    	$User->save();
        return redirect('');
    }


     /**
     * Change Email.
     *
     * @return void
     */


    public function changeEmail(Request $req)
    {
        $User = new User;
        $User->currentemail=$req->currentemail;
        $User->newemail=$req->newemail;
    	$User->save();
        return redirect('');
    }



     /**
     * Reset User Password.
     *
     * @return void
     */

    public function resetPassword(Request $req)
    {

        $User = new User;
    	$User->email=$req->email;
        $User->token=$req->token;
    	$User->save();
    	return redirect('');
    }

    public function showProfile()
    {

        $data = User::find(2);
        //return view('',['data'=>$data]);

    }
     /**
     * User Update Profile.
     *
     * @return void
     */

    public function updateProfile(Request $req)
    {
        $update =User::find(2);
    }


}
