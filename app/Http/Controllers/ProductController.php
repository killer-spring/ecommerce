<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Utils\AppConst;
use Illuminate\Http\Request;
use GuzzleHttp\Handler\Proxy;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use App\Http\Requests\ProductRequest;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Redirect;

class ProductController extends Controller
{
    private $_product;

    public function __construct(Product $product){
        $this->_product = $product;
    }

    /**
	 * Show the form for creating a new product
	 *
	 * @return Response
	 */
	public function create()
	{
        $product = new Product;
        return  view('admin.add-product',compact('product'));
	}

    /**
     * Addd product function
     *
     * @param Request $request
     * @return void
     */
    public function saveProduct(Request $request)
    {
        $this->validate($request, [
        'image' => 'required|image|mimes:jpeg,png,jpg,bmp,gif,svg|max:2048',
        'title' => 'required',
        'category_id' =>   'required',
        'unit_price' => 'required',
        'quantity' => 'required',
        'size' => 'required',
        'color' => 'required',
        'weight' => 'required',
        'is_trending' => 'required',
        'description' => 'required',
        ]);
        $product = new Product;
        $product->title=$request->title;
        $product->unit_price=$request->unit_price;
        $product->user_id =auth()->user()->id;
        $product->quantity=$request->quantity;
        $product->size=$request->size;
        $product->color=$request->color;
        $product->weight=$request->weight;
        $product->category_id = $request->category_id;
        $product->is_trending = $request->is_trending;
        $product->description=$request->description;

        if ($request->hasFile('image'))
        {
            $image = $request->file('image');
            $imageName = time().".".$image->extension();
            $request->file('image')->storePubliclyAs('/public',$imageName);
            $product->image = Storage::url($imageName);
            $product->save();
            return back()->with('success','Image Upload successfully');



            // $image = $request->file('image');
            // $imageName = time().".".$image->extension();
            // $destinationPath = public_path('/assets/img/product');
            // $product->image = '/assets/img/product/'.$imageName;
            // $image->move($destinationPath, $imageName);
            // $product->save();
            // return back()->with('success','successfully ');


        }
    }
    /**
     * fatch profucts function
     *
     * @return object
     */
    public function productsListForAdmin()
    {
        $p = new Product();
        $allProduct = $this->_product->fetchProducts();
        return view('Admin.view-products',compact('allProduct'));
    }

    public function productsForPublic()
    {
        $allPro = $this->_product->listOfProducts();
        return view('home.index',compact('allPro'));
    }

    public function productDetails($id)
    {
        $product= Product::find($id);
        return view('user/product-details',compact('product'));
    }

    /**
     * delete product function
     *
     * @param integer $id
     * @return void
     */
    public function deleteProduct($id)
    {
        $product = Product::find($id);
        $image_path = $product['image'];  // Value is not URL but directory file path

        if(File::exists($image_path))
        {
            File::delete($image_path);
        }

        $product->delete();
        return redirect('/products');
    }
    /**
     * get a product details function
     *
     * @param integer $id
     * @return void
     */
    public function showDetails($id)
    {
        $product = Product::find($id);
        return view('admin.edit-product',compact('product'));
    }
    /**
     * update a product details function
     *
     * @param Request $request
     * @return void
     */
    public function updateProduct(Request $request){

        $product =Product::find($request->id);

        $image_path = $product['image'];
//         $exists = Storage::exists($image_path);
// dd($image_path);
        // Value is not URL but directory file path

        // if(Storage::exists('storage/download.png')) {
            Storage::setVisibility('storage/1607260203.png');
        dd(Storage::delete('storage/1607257006.png'));
        // dd('ok');
        // }else {
            // dd('no');
        // }

        // $this->validate($request, [
        // 'image' => 'required|image|mimes:jpeg,png,jpg,bmp,gif,svg|max:2048',
        // 'title' => 'required',
        // 'category_id' =>   'required',
        // 'unit_price' => 'required',
        // 'quantity' => 'required',
        // 'size' => 'required',
        // 'color' => 'required',
        // 'weight' => 'required',
        // 'is_trending' => 'required',
        // 'description' => 'required',
        // ]);

        // $product->title=$request->title;
        // $product->unit_price=$request->unit_price;
        // $product->user_id =auth()->user()->id;
        // $product->image =$request->image;
        // $product->quantity=$request->quantity;
        // $product->size=$request->size;
        // $product->color=$request->color;
        // $product->weight=$request->weight;
        // $product->category_id = $request->category_id;
        // $product->is_trending = $request->is_trending;
        // $product->description=$request->description;

        // if ($request->hasFile('image'))
        // {
        //      $image = $request->file('image');
        //     $imageName = time().".".$image->extension();
        //     $destinationPath = public_path('/assets/img/product');
        //     $product->image = '/assets/img/product/'.$imageName;
        //     $image->move($destinationPath, $imageName);
        //     $product->save();
        //      return redirect('products')->with('success','successfully Update');
        // }
    }

    /**
	 * Display a listing of products
	 *
	 * @return Response
	 */
	public function index()
	{

    }

	/**
	 * Store a newly created product in storage.
	 *
	 * @return Response
	 */
	public function store(ProductRequest $request)
	{
        dd($request->title);
        $request->save();
        // Auth::user()->create($request);

	}

	/**
	 * Display the specified product.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{

	}

	/**
	 * Show the form for editing the specified product.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{

	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{

	}
}
