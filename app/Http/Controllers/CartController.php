<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

class CartController extends Controller
{
    function add(Product $product)
    {

        // add the product to cart
        \Cart::session(auth()->id())->add(array(
            'id' => $product->id,
            'name' => $product->title,
            'price' => $product->price,
            'quantity' => 1,
            'attributes' => array(),
            'associatedModel' => $product
        ));



        return redirect()->route('cartItem');

    }
    function cartItem()
    {
        $cartItems = \Cart::session(auth()->id())->getContent();
        return view('user.view-cart',compact('cartItems'));
    }
}
