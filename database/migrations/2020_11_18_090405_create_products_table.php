<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->integer('category_id');
            $table->integer('sub_category_id');
            $table->integer('user_id');
            $table->string('title', 60);
            $table->string('image', 100)->nullable();
            $table->decimal('unit_price', 12, 2)->default(0.0);
            $table->smallInteger('quantity')->default(0);
            $table->tinyInteger('size')->nullable();
            $table->string('color')->nullable();
            $table->tinyInteger('weight')->nullable();// weight in grams and kg
            $table->boolean('is_trending')->default(false);
            // $table->boolean('is_new_arrival')->default(true);
            $table->text('description')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
