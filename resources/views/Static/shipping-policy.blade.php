@extends('master')

@section('shipping-policy')

<div class="container text-justify pb-50 pt-20">
    <center><h3><b> Shipping Policy</b></h3></center><br>

    <h4>Returns Policy </h4>
    <p> <b>1:</b> If your product is damaged, defective, incorrect or incomplete at the time of delivery, please file a return request on the app or website within 14 days for Dmall items and 7 days for non-Dmall items of the delivery date. </p>
    
    <p> <b>2:</b> For selected categories, we accept a change of mind: Men’s Fashion, Women’s Fashion, Men’s bags, Women’s bags, Luggage & Suitcase, Bedding, Bath. Exceptions are women's intimate apparel, men's innerwear, swimwear, eyewear, jewellery, watches, duffel bags, shoe-care.</p>
    
    <p> <b>3:</b>For device related issues after usage or the expiration of the return window, seller warranty or brand warranty could be given by the seller. For seller warranty, please contact the seller. The contact details of the seller can be found on the invoice. For brand warranty, please refer to the <b>Brand Contact List.</b> For more information on warranty claims please view our <b>Warranty Policy.</b> </p>
   <br>
    <h4>I have a complaint about the courier executive who came to deliver my order, who should I reach out? </h4>
    <p>Daraz strives to ensure that customers enjoy the ultimate shopping experience on the platform, and reap the benefits of online shopping while experiencing true customer delight. </p>
    <p>Our teams are working round the clock to ensure that your experience is as good as it could be, and really appreciate you for visiting the Help Center. We have dedicated a team member to follow up with our Delivery Partner with regards to the concern with the rider. Feel free to click on the <b>Live Chat between 9 a.m to 9 p.m</b> and we'd b happy to assist you. </p>
    <br>
    <h4>How do I track my order?</h4>
    <p>To track your order, simply head over to </p>

    <br>
    <h4>What are the common reasons for delay?</h4>
    <p> The most <b>common reasons </b> for delivery delays are:</p>
    <p> <b>1)  Seller Sourcing Issues:</b> The seller could take longer than expected time to fulfill your order.</p>
    <p> <b>2) Courier service delay:</b> Either DEX or one of our 3PL partners is taking longer than expected time to deliver your order.</p>
    <p><b>3) Cross Border shipment delay:</b> Due to Air Transportation or Custom delays, your Cross Border order may take longer than usual to be delivered.</p>
    <p><b>4) Customer not available:</b>   Our Delivery Hero has not been able to get in touch with the customer.</p>
    <p> <b> 5) Wrong Address / Phone Number:</b> While placing an order the customer has entered an incorrect address or phone number, leading to a communication barrier with the customer.</p>
    <p><b> 6) Extreme Weather:</b>  The weather conditions of the destined city may be rough, for example, heavy rainfall, smog, landslide, etc. which may result in a delivery hold-up.</p>
    <p><b> 7) Law and Order conditions:</b> Due to religious procession, lockdowns, strike issues resulting in route blocking, you may receive your package after the promised time.</p>
    
    
    <br>
    <h4>I missed my package delivery, what should I do now?</h4>
    <p>If you missed the delivery, we’d attempt to <b>deliver your order again.</b> The rider may contact you to confirm your availability or location. </p>
    <p> We will attempt the delivery of your order for <b>3 times</b> before canceling it.</p>
    

    
    <br>
    <h4>What are the delivery charges?</h4>
    <p>Please know that the delivery charges are visible on the final checkout page and are calculated based on the following:</p>
    <ol start="1">
    <li>Number of items in the order</li>
    <li>Weight of the items</li>
    <li>Delivery Location</li>
    </ol>
    <p> If you include <b>multiple items</b> in your order, then certain <b>caps on charges</b> may apply depending on the <b>category</b> and <b>weight.</b></p>
    
    <br>
    <h4>What are the promised delivery timelines?</h4>
    <p>We always try our best to deliver your order at the earliest! Find the delivery time on every product page in the section <b>Delivery.</b> </p>
    </div>
@endSection