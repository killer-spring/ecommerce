@extends('master')

@section('terms-conditions')



<div class="container text-justify pb-50 pt-20">
    <center><h3><b> Terms and Conditions</b></h3></center>
    <br>
    <p> This page contains links to current corporate policies as well as terms and conditions for the products 
    and services available through Outstock. To view any of the documents presented on this page, click on the tems & conditions
    . </p>

    <br>
    <p>Thank you for trading on <b>Outstock</b> web-based transaction platforms.  
    This Transaction Services Agreement (this “Agreement”) describes the terms and conditions
     on which you conclude online transactions for products and services by using the websites,
      mobile sites, mobile applications and other online transaction portals owned, operated,
       branded or made available by <b>Outstock</b> (defined below) from time to time which relate 
       to (a) the <b>Outstock.com</b> e-commerce platform including but not limited to the web and
        mobile-optimized versions of the websites identified by the uniform resource locator 
        <b>"Outstock.com"</b> and the mobile applications of the Outstock e-commerce platform 
        (the Outstock Sites”); and (b) the Outstock e-commerce platform including but not limited
         to the web and mobile-optimized versions of the websites identified by the uniform resource 
         locators <b>"Outstock.com"</b>, <b>"Outstock.com"</b> and “www.tmall.ru”, and the mobile 
         applications of the AliExpress e-commerce platforms, (collectively the “AliExpress Sites”),
          (with Outstock Sites and <b>Outstock.com</b> Sites collectively the “Sites”).  This Agreement contains 
          various limitations on Alibaba.com’s transaction services offered through the Sites as well as
           gives various powers and authority to Outstock.com with respect to online transactions effected 
           using Outstock.com’s transaction services offered through the Sites.  This includes without 
           limitation the power and authority to reject or cancel an online transaction, to refund the 
           funds to a buyer or to release the funds to a seller.  You should read this Agreement and, to
            the extent as applicable, the relevant Transactional Terms (defined in clause 1.2 below), 
            and other rules and policies of Alibaba.com (including those specified in clause 1.3), as
             well as (i) the Alipay Services Agreement made between you as a User (as defined below) and
              Alipay Singapore E-Commerce Private Limited (“Alipay”) (the “Alipay Services Agreement”), 
              and (ii) the Outstock.com Supplemental Services Agreement between you as a User and Outstock.com
               (as defined below) (the <b>Outstock.com</b> Supplemental Services Agreement”) carefully which are
                hereby incorporated into this Agreement by reference. </p>

            <h6>1.  Application and Acceptance of Term</h6>
            <p>1.1 Contracting Party. This Agreement is entered into between you (also referred to as “Member” hereinafter)
             and the Outstock.com contracting entity determined in accordance with this clause 1.1 ("Outstock.com” or 
             “we”) for use of Alibaba.com’s certain transaction services 
            offered through the Sites as described hereunder</p>

            <p>If you are from outside mainland China, you are contracting with Outstock.com Singapore E-Commerce 
            Private Limited (incorporated in Singapore with Company Reg. No. 200720572D). </p>
            <p>If you are from mainland China, you are contracting with Outstock.com Singapore E-Commerce
             Private Limited and Outstock (China) Technology Co Ltd.  Alibaba (China) Technology Co Ltd provides 
             technical support in connection with the transaction services utilizing its strengths in network 
             technologies, etc</p>
            <p>Notwithstanding anything to the contrary in the forgoing provisions of this clause 1.1, 
            if you are resident in or access and use the AliExpress Sites from any of the Relevant Jurisdictions,
             you are contracting with Alibaba.com Singapore E-Commerce Private Limited. Outstock.com Singapore 
             E-Commerce Private Limited provides Transaction Services to Members who sell to buyers from any of the <b>Relevant Jurisdictions.</b> Outstock.com Singapore E-Commerce Private Limited is a service provider for AliExpress Russia Holding Private Limited (incorporated in Singapore with Company Reg. No. 201917627W). “Relevant Jurisdictions” shall mean the Russian Federation, Azerbaijan, Armenia, Belarus, Georgia, Kazakhstan, Kyrgyzstan, Moldova, Turkmenistan, Tajikistan and Uzbekistan.</p>

             <h6>5.  Member’s Responsibilities</h6>
             <p>5.1 Provision of Information and Assistance.  You agree to give all notices, 
             provide all necessary information, materials and approval, and render all reasonable
              assistance and cooperation necessary for the completion of the Online Transactions
               and Outstock.com’s provision of the Transaction Services.  If your failure to do so
                results in delay in the provision of any Transaction Service, cancellation of any 
                Online Transaction, or disposal of any funds, Outstock.com shall not be liable for 
                any losses or damages arising from such default, delay, cancellation or disposal.
            </p>
            <br>
            <br>
            <p>(a)  you will use the Transaction Services in good faith and in compliance with all applicable laws and regulations，including laws related to anti-money laundering and counter-terrorism financing;</p>
            <p>(b)  all information and material you provide in connection with the use of the Transaction Services is true, lawful and accurate, and is not false, misleading or deceptive;</p>
            <p>(c)  you will not use the Transaction Services to defraud Alibaba.com, our affiliates, or other members or users of the Sites or engage in other unlawful activities (including without limitation dealing in products prohibited by law); and</p>
            <p>(d)  in case that you are a Seller of products, you have the legitimate right and authorization to sell, distribute or export the products using the Transaction Services and such products do not infringe any third party’s rights; and</p>
            <p>(e)  in case that you are a Seller of products, you have good title to the products ordered under the Online Transaction, and the products meet the agreed descriptions and requirements</p>
            <br>
            <br>
            <p>5.3 Breaches. If you are, in Outstock.com’s opinion, not acting in good faith, abusing the 
            Transaction Services, or otherwise acting in breach of this Agreement, Outstock.com shall have 
            the right to cancel the relevant Online Transaction(s) at any time without any liability for any
             losses or damages arising out of or in connection with such cancellation.  Outstock.com also reserves
              the right to impose any penalty, or to temporarily or permanently suspend or terminate your use of
               the Transaction Services, temporarily or permanently suspend or terminate or procure the suspension 
               or termination of your paid or free membership on the Sites.  Outstock.com also reserves the right to
                (i) temporarily suspend the transaction functionalities of your account with Alibaba.com for a 
                prescribed period determined by Outstock.com, or permanently terminate the use of your Outstock.com 
                account and/or (ii) authorize Alipay to temporarily suspend the transaction functionalities of your
                 Alipay account for a prescribed period determined by Alibaba.com, or permanently terminate the use
                  of your Alipay account without any liability for any losses or damages arising out of or in 
                  connection with such suspension or termination.  Outstock.com may also publish the findings,
             penalties and other records regarding the breaches on the Sites.</p>
             <br>
            <br>
             <p>11.2 Severance. If any provision of this Agreement is held to be invalid or unenforceable, such provision shall be deleted and the remaining provisions shall remain valid and be enforced.</p>
             <br>
            <br>
             <p>11.3 Headings. Headings are for reference purposes only and in no way define, limit, construe or describe the scope or extent of such section.</p>
             <br>
            <br>
             <p>11.4 Independent Contractor. No agency, partnership, joint venture, employee-employer or franchiser-franchisee relationship is intended or created by this Agreement.</p>
             <br>
            <br>
             <p>11.6 Assignment. Alibaba.com shall have the right to assign this Agreement (including all of our rights, titles, benefits, interests, and obligations and duties in this Agreement) to any of our affiliates and to any successor in interest.  Alibaba.com may delegate certain of Alibaba.com rights and responsibilities under this Agreement to independent contractors or other third parties.  You may not assign, in whole or part, this Agreement to any person or entity.</p>
            <hr>
</div>  


@endSection