@extends('master')

@section('delivery-information')


<div class="container text-justify pb-50 pt-20">
    <center><h3><b> Delivery Information</b></h3></center>
    <br>
    <h6>Can I cancel my order for overseas products? </h6>
    <p>Once your order is marked as shipped and enters the shipping parameters,
     Daraz cannot halt the delivery of the product since the import process has started.
     <b> Therefore you cannot cancel your order once it has been shipped.</b>
You can contact us via live chat if the order is not shipped, we will facilitate you accordingly.</p>
<br>

<h6>I got a notification stating my order has been cancelled. What do I do now? </h6>
<p> To know the reason for Cancellation, click the <b>Notification</b> that you have received or for more 
details on <b>cancellation</b>, click <b>Account</b>, and select Cancellation.

You will be redirected to a page where you can view your cancelled order. Click on the order
 that you want to check. The details and reason for the cancellation will be stated there.</p>
 <p>

<b>NOTE:</b> For COD orders, you need to re-order, and for prepaid order check the <b>Refund Timeline.</b></p>
<br>
<h6>I accidentally ordered duplicate items. What should I do?</h6>
    <p>If you have accidentally ordered duplicate items, you may cancel the order if it's at <b>Payment Pending</b> or <b>Processing Stage.</b></p>
    <p>If the order status is <b>Shipped</b> and you are <b>NOT</b> able to track the order through the Consignment Number, then you are requested to contact us via <b>live chat between 9 a.m. to 9 p.m.</b></p>
    <p>If the order status is <b>Shipped</b> and you are able to track the order through the Consignment Number, then you are requested to <b>refuse the order when it is delivered to you.</b></p>
    <br>
    <h6>What do the different order statuses in 'My Order' mean? </h6>
    <p><b>Processing:</b> Your order has been entered in the order pool of Daraz. Once confirmed by the seller, it will be delivered within a given timeline. </p>
    <p><b>Shipped:</b> Your order has been received from the seller and is on its way via courier. It will be delivered to you as per the standard delivery time. You may track your order from your Daraz App,<b>Account > View All > Track Package.</b> </p>
    <p><b>Delivered:</b> Your order has been delivered to you. </p>
    <p><b> Canceled: </b>Your order has been marked canceled. The details and reason for the cancellation will be stated once you click the Cancellation Notification. </p>

    <br>
    <h6>How to cancel the order?</h6>
    <p>Cancellations are easy! In order to cancel any order please follow the steps below:</p>
    <p><b> 1: </b>Click View All in the My Orders section on the Account page of your Daraz App</p>
    <p><b>2: </b> Click on the order you want to cancel, select Cancel</p>
    <p><b>3: </b> Select Cancellation Reason, click Confirm.</p>
    <p><b>4: </b>  Write in your comments in the Additional Comments field and click Submit to confirm your cancellation.</p>
    <p><b>NOTE: </b>  Daraz’s cancellation policy states that you can cancel your order from your Daraz account before the order is shipped out from Daraz’s warehouse or has been dispatched by the seller. 
    If your order is already on its way to be shipped, you can contact us on <b>live chat</b> from <b>9 a.m. to 9 p.m.</b></p>
    <br>

    <h6>I got a notification stating my order has been cancelled. What do I do now?</h6>

    <p>To know the reason for Cancellation, click the <b>Notification</b> that you have received or for more details on cancellation, click <b>Account,</b> and select <b>Cancellation.</b>
 </p>

 <b>NOTE:</b> For COD orders, you need to re-order, and for prepaid order check the <b>Refund Timeline.</b></p>
   <br>
 <h6>What is Outstock's order cancellation policy?</h6>
 <p>Canceling your orders on Daraz is easy and hassle-free. <b>Outstock cancellation policy states that you can cancel your order from your Daraz account before the order is shipped out from Daraz’s warehouse or has been dispatched by the seller.</b> </p>
   <br  >
 <h6>How do I "Review" my product?</h6>

 <p> After receiving the parcel, you may <b>Review the product(s)</b> based on your satisfaction level. </p>
  <p> <b>You can rate products with stars like these!</b></p>
<p>Five Stars – I Love it ★★★★★ </p>
<p>Four Stars – I Like It ★★★★ </p>
<p>Three Stars – It’s OK ★★★ </p>
<p>Two Stars – I Don’t Like It ★★ </p>
<p>One star – I hate It ★ </p>
</b>
    </div>
@endSection