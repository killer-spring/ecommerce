@extends('master')

@section('privacy-policy')



<div class="container text-justify pb-50 pt-20">
    <center><h3><b> Privacy Policy</b></h3></center><br>
    <p>
        On the legal side of things, if you want to make your Online Business well protected and increase trust between you and your customer base, 
        having a Privacy Policy is very important. While it should be a legal document, it should also be easy to understand for a customer and/or visitor that interacts with your website.

        To help you get started, we have created a sample Privacy Policy that you can use by inserting your relevant details and publishing it on your Website.

</p>
<hr>
<h4> WHAT PERSONAL INFORMATION WE COLLECT </h4>
<p>When you visit the Site, we automatically collect certain information about your device, including information about your web browser, IP address, time zone, and some of the cookies that are installed on your device.

Additionally, as you browse the Site, we collect information about the individual web pages
or products that you view, what websites or search terms referred you to the Site, and information
about how you interact with the Site. We refer to this automatically collected information as
<b>Device Information.</b> 

</p>
<hr>

<p>Mention all other tracking tools and/or technologies being used by your website.

Also, when you make a purchase or attempt to make a purchase through the Site, 
we collect certain information from you, including your name, billing address, 
shipping address, payment information (including credit card numbers Mention 
all types of accepted payments, email address, and phone number. This is called <b>Order Information.</b>

Make sure you mention all other information that you collect.

By <b>Personal Information</b> in this Privacy Policy, we are talking both about 
Device Information and Order Information. </p>
<hr>
<h4> HOW DO WE USE YOUR PERSONAL INFORMATION </h4>

<p> 
    We use the Order Information that we collect generally to fulfil 
    any orders placed through the Site (including processing your payment 
    information, arranging for shipping, and providing you with invoices 
    and/or order confirmations).

 Additionally, we use this Order Information to: - Communicate with you. - Screen our
 orders for potential risk or fraud. - When in line with the preferences you have 
 shared with us, provide you with information or advertising relating to our products or services.

We use the Device Information that we collect to help us screen for potential risk and
fraud (in particular, your IP address), and more generally to improve and optimize our Site.  </p>
<hr>

<h4> SHARING YOUR PERSONAL INFORMATION </h4>

<p> We share your Personal Information with third parties to help us use your Personal
    Information, as described above.

We also use Google Analytics to help us understand how our customers use (Store Name).
<b>How Google uses your Personal Information.</b>

Finally, we may also share your Personal Information to comply with applicable laws
and regulations, to respond to a subpoena, search warrant or other lawful requests 
for information we receive, or to otherwise protect our rights.   </p>
<hr>

<h4> YOUR RIGHTS </h4>

<p>If you are a European resident, you have the right to access the personal 
  information we hold about you and to ask that your personal information is 
  corrected, updated, or deleted. If you would like to exercise this right, please contact us.

Additionally, if you are a European resident we note that we are processing your
 information in order to fulfil contracts we might have with you (for example if 
 you make an order through the Site), or otherwise to pursue our legitimate business
 interests listed above.
 Please note that your information will be transferred outside of Europe, including
 to Canada and the United States.
    
</p><hr>

<h4> DATA RETENTION </h4>

<p> 
    When you place an order through the Site, we will maintain your
    Order Information for our records unless and until you ask us to delete this information.


<h5>MINORS</h5>

<p>The Site is not intended for individuals under the age of (CLEARLY MENTION AGE).</p>

<h5>CHANGES</h5>

We may update this privacy policy from time to time in order to reflect, for example,
changes to our practices or for other operational, legal or regulatory reasons.

If you have questions and/or require more information, do not hesitate to contact us
 <b>(Add Relevant contact information).</B>  </p>





</div>


@endSection