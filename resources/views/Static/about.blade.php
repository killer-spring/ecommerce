@extends('master')

@section('about')

<title>About</title>
        <!-- Content Wrapper -->
            <article class="about-page" style="margin-top:150px;"> 
                <!-- Breadcrumb -->
                <section class="theme-breadcrumb pad-50">                
                    <div class="theme-container container ">  
                        <div class="row">
                            <div class="col-sm-8 pull-left">
                                <div class="title-wrap">
                                    <h2 class="section-title no-margin">About us</h2>
                                    <p class="fs-16 no-margin">Know about us more</p>
                                </div>
                            </div>
                              
                        </div>
                    </div>
                </section>
                <!-- /.Breadcrumb -->

                <!-- About Us -->
                <section class="pad-50 about-wrap">
                 
                    <div class="theme-container container">               
                        <div class="row">
                            <div class="col-md-6">
                                <div class="about-us pt-10">
                                    <p class="fs-16 wow fadeInUp" data-wow-offset="50" data-wow-delay=".25s">
                                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam 
                                        nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam 
                                        erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci 
                                        tation ullamcorper suscipit lobortis nisl ut aliquip.
                                    </p>
                                    <ul class="feature">
                                        <li> 
                                            <img alt="" src={{ URL::to('assets/icons/icon-2.png')}} class="wow fadeInUp" data-wow-offset="50" data-wow-delay=".20s" /> 
                                            <div class="feature-content wow rotateInDownRight" data-wow-offset="50" data-wow-delay=".30s"> 
                                                <h4 class="title-1">Fast delivery</h4> 
                                                <p>Duis autem vel eum iriure dolor</p>                                            
                                            </div>  
                                        </li>
                                        <li> 
                                            <img alt="" src={{ URL::to('assets/icons/icon-3.png')}} class="wow fadeInUp" data-wow-offset="50" data-wow-delay=".20s" /> 
                                            <div class="feature-content wow rotateInDownRight" data-wow-offset="50" data-wow-delay=".30s"> 
                                                <h4 class="title-1">Secured service</h4> 
                                                <p>Duis autem vel eum iriure dolor in hendrerit</p>                                            
                                            </div>  
                                        </li>
                                        <li> 
                                            <img alt="" src={{ URL::to('assets/icons/icon-4.png')}} class="wow fadeInUp" data-wow-offset="50" data-wow-delay=".20s" /> 
                                            <div class="feature-content wow rotateInDownRight" data-wow-offset="50" data-wow-delay=".30s"> 
                                                <h4 class="title-1">Worldwide shipping</h4> 
                                                <p>Eum iriure dolor in hendrerit in vulputa</p>                                            
                                            </div>  
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-6 text-center">                                
                                <img alt="" src={{ URL::to('assets/img/about-img.png')}} class="effect animated fadeInRight" />
                            </div>
                        </div>
                    </div>
                </section>
                <!-- /.About Us -->

                <!-- More About Us -->
                <section class="pad-30 more-about-wrap">
                    <div class="theme-container container pb-100">               
                        <div class="row">
                            <div class="col-md-4 col-sm-4 wow fadeInUp" data-wow-offset="50" data-wow-delay=".20s">
                                <div class="more-about clrbg-before">
                                    <h2 class="title-1">what we do</h2>
                                    <div class="pad-10"></div>
                                    <p>Lorem ipsum dolor sit amet, consectetuer 
                                        adipiscing elit, sed diam nonummy nibh eui
                                        tincidunt ut laoreet dolore magna aliquam
                                        volutpat. Ut wisi enim ad minim veniam, quis 
                                        nostrud exerci tation ullamcorper suscipit 
                                        lobortis nisl ut aliquip ex ea commodo</p>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 wow fadeInUp" data-wow-offset="50" data-wow-delay=".30s">
                                <div class="more-about clrbg-before">
                                    <h2 class="title-1">Our History</h2>
                                    <div class="pad-10"></div>
                                    <p>Lorem ipsum dolor sit amet, consectetuer 
                                        adipiscing elit, sed diam nonummy nibh eui
                                        tincidunt ut laoreet dolore magna aliquam
                                        volutpat. Ut wisi enim ad minim veniam, quis 
                                        nostrud exerci tation ullamcorper suscipit 
                                        lobortis nisl ut aliquip ex ea commodo</p>
                                </div>
                            </div>
                            <div class="col-md-4  col-sm-4 wow fadeInUp" data-wow-offset="50" data-wow-delay=".40s">
                                <div class="more-about clrbg-before">
                                    <h2 class="title-1">our mission</h2>
                                    <div class="pad-10"></div>
                                    <p>Lorem ipsum dolor sit amet, consectetuer 
                                        adipiscing elit, sed diam nonummy nibh eui
                                        tincidunt ut laoreet dolore magna aliquam
                                        volutpat. Ut wisi enim ad minim veniam, quis 
                                        nostrud exerci tation ullamcorper suscipit 
                                        lobortis nisl ut aliquip ex ea commodo</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- /.More About Us -->
            </article>
            <!-- /.Content Wrapper -->






        </main>

@endSection