<div class="mt-3">
    <label>Product Name</label><br/>
        <input  type="text" value="{{old('title', $product->title)}}" name="title" class="form-control @error('title') is-invalid @enderror">
        @error('title')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
</div>
<div class="mt-3">
    <label>Description</label>
        <textarea type="text" name="description" class="form-control @error('description') is-invalid @enderror" rows="8" >{{old('description', $product->description)}}</textarea>
            @error('description')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
</div>
<div class="mt-3">
    <label>Category</label>
        <select name="category_id" class="form-control @error('category_id') is-invalid @enderror">
            <option value="">Select</option>
            <option value="1">Jewlery</option>
            <option value="2">Baby Toys</option>
            <option value="3">Sports</option>
        </select>
            @error('category_id')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
</div>
<div class="row mt-3">
        <div class="col-4">
            <label>Unit Price</label><br/>
            <input  type="text" name="unit_price" value="{{old('unit_price', $product->unit_price)}}" class="form-control @error('unit_price') is-invalid @enderror">
            @error('unit_price')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="col-4">

            <label>Size</label>
            <select name="size" id="" class="form-control @error('size') is-invalid @enderror">
                <option value="">Select</option>
                <option value="1">S</option>
                <option value="2">M</option>
                <option value="3">L</option>
                <option value="4">XL</option>
                <option value="5">XXL</option>
            </select>
            @error('size')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
<div class="col-4">
    <label>Color</label>
        <select name="color" id="" class="form-control @error('color') is-invalid @enderror">
            <option value="">Select</option>
            <option value="1">Red</option>
            <option value="2">Green</option>
            <option value="3">Blue</option>
            <option value="4">Yellow</option>
            <option value="5">Orange</option>
            <option value="6">Purple</option>
            <option value="7">Black</option>
            <option value="8">White</option>
        </select>
        @error('color')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
</div>
</div>
<div class="row mt-3">
    <div class="col-4">
        <label>Units In Stock</label><br/>
        <input  type="text" name="quantity" value="{{old('quantity', $product->quantity)}}" class="form-control @error('quantity') is-invalid @enderror">
        @error('quantity')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>
    <div class="col-4">
        <label>Is Trending</label>
        <select name="is_trending" id="" class="form-control @error('is_trending') is-invalid @enderror">
            <option value="">Select</option>
            <option value="1">Yes</option>
            <option value="2">No</option>
        </select>
        @error('is_trending')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>
    <div class="col-4">
        <label>Weight</label>
        <div class="row">
            <div class="col-12">
                <input type="text" name="weight" value="{{old('weight', $product->weight)}}" class="form-control col-5 @error('weight') is-invalid @enderror">
                /g
                @error('weight')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
        </div>
    </div>
</div>
<div class="row mt-3">
    <div class="col-12">
        <div class="form-control btn btn-primary @error('image') is-invalid @enderror">
            <span>Choose file</span>
            <input class="" type="file" name="image" value="{{old('image', $product->image)}}">
        </div>
        @error('image')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
            <div class="file-path-wrapper">
            <!-- <input class="file-path validate" type="text" placeholder="Upload your file"> -->
            </div>
    </div>
</div>
<div>
    <hr>
    <button class="form-control mt-2 font-weight-bold mb-5" style="background-color:#FB9678">Add Product Now</button>
</div>
