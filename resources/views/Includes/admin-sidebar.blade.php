
 <header>
     {{-- <link rel="stylesheet" href={{ URL::to('assets/css/bootstrap.min.css')}}> --}}
    <link rel="icon"  sizes="16x16" href={{URL::to('assets/img/favicon.png')}}>
    <link href={{URL::to('assets/admin/css/morris.css')}} rel="stylesheet">
    <link href={{URL::to('assets/admin/css/ecommerce.css')}} rel="stylesheet">
    <!-- Custom CSS -->
    <link href={{URL::to('assets/admin/css/style.min.css')}} rel="stylesheet">
    <link rel="stylesheet" href={{ URL::to('assets/css/preloader.css')}}>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
 </header>

 <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->

         <header class="topbar">
            <nav style="margin-left:30px; margin-top:10px;">
                <!-- ============================================================== -->
                <!-- Logo -->
                <!-- ============================================================== -->
                <div class="navbar-header">
                    <a class="navbar-brand" href="">
                         <div class="logo">
                                <a href=""><img src="assets/img/logo/logo-2.png" alt="logo"></a>
                            </div>

                </div>


            </nav>
        </header>
        <aside class="left-sidebar" style="background-color: #2B2B2B">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- User Profile-->
                <div class="user-profile">
                    <div class="user-pro-body">

                        <div> <a href=""><img src={{('assets/img/user.png')}} alt="logo"></a></div>
                        <div class="dropdown">
                        <a href="javascript:void(0)" class=" u-dropdown link hide-menu text-white" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{auth()->user()->name}}<span class="caret"></span></a>

                        </div>
                    </div>
                </div>
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <li>
                            <div class="hide-menu text-center text-white">
                                <div id="eco-spark"></div>
                                <small>TOTAL EARNINGS - JUNE 2020</small>
                                <h4>$2,478.00</h4>
                            </div>
                        </li>
                        <li class="nav-small-cap" style="margin-left:55px;">-- OVERVIEW --</li>
                        <li> <a class=" waves-effect waves-dark" href="{{URL::to('dashboard')}}" aria-expanded="false"><i class="fa fa-tachometer"></i><span class="hide-menu">Dashboard</span></a>

                            
                        </li>
                        <li> <a class=" waves-effect waves-dark" href="/products" aria-expanded="false"><i class="fa fa-product-hunt"></i><span class="hide-menu">View Products</span></a>

                        </li>
                        <li> <a class=" waves-effect waves-dark" href="{{URL::to('add-product')}}" aria-expanded="false"><i class="fa fa-plus-circle"></i><span class="hide-menu">Add Product</span></a></li>
                        <li> <a class=" waves-effect waves-dark" href="{{URL::to('allcategory')}}" aria-expanded="false"><i class="fa fa-plus-circle"></i><span class="hide-menu">Add Category</span></a></li>


                        <li> <a class=" waves-effect waves-dark" href="{{URL::to('orders-list')}}" aria-expanded="false"><i class="fa fa-list"></i><span class="hide-menu">Orders List</span></a>

                        </li>
                        <li> <a class=" waves-effect waves-dark" href="{{URL::to('users')}}" aria-expanded="false"><i class="fa fa-users"></i><span class="hide-menu">Users</span></a>

                        </li>
                        <li> <a class=" waves-effect waves-dark" href="{{URL::to('notifications')}}" aria-expanded="false"><i class="fa fa-bell"></i><span class="hide-menu">Notifications</span></a>

                        </li>
                        <li> <a class=" waves-effect waves-dark" href="{{URL::to('logout')}}" aria-expanded="false"><i class="fa fa-sign-out"></i><span class="hide-menu">Logout</span></a>

                        </li>








                        <li> <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="ti-pie-chart"></i><span class="hide-menu">Charts</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="chart-morris.html">Morris Chart</a></li>
                                <li><a href="chart-chartist.html">Chartis Chart</a></li>
                                <li><a href="chart-echart.html">Echarts</a></li>
                                <li><a href="chart-flot.html">Flot Chart</a></li>
                                <li><a href="chart-knob.html">Knob Chart</a></li>
                                <li><a href="chart-chart-js.html">Chartjs</a></li>
                                <li><a href="chart-sparkline.html">Sparkline Chart</a></li>
                                <li><a href="chart-extra-chart.html">Extra chart</a></li>
                                <li><a href="chart-peity.html">Peity Charts</a></li>
                            </ul>
                        </li>

                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </aside>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->


    <script src={{('assets/admin/js/jquery-3.2.1.min.js')}}></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src={{('assets/admin/js/popper.min.js')}}></script>
    <script src={{('assets/admin/js/bootstrap.min.js')}}></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src={{('assets/admin/js/perfect-scrollbar.jquery.min.js')}}>></script>
    <!--Wave Effects -->
    <script src={{('assets/admin/js/waves.js')}}>></script>
    <!--Menu sidebar -->
    <script src={{('assets/admin/js/sidebarmenu.js')}}>></script>
    <!--stickey kit -->
    <script src={{('assets/admin/js/sticky-kit.min.js')}}>></script>
    <script src={{('assets/admin/js/jquery.sparkline.min.js')}}>></script>
    <!--Custom JavaScript -->
    <script src={{('assets/admin/js/custom.min.js')}}>></script>
    <script src={{('assets/admin/js/jquery.sparkline.min.js')}}>></script>
    <!-- ============================================================== -->
    <!-- This page plugins -->
    <!-- ============================================================== -->
    <!--morris JavaScript -->
    <script src={{('assets/admin/js/raphael-min.js')}}>></script>
    <script src={{('assets/admin/js/morris.min.js')}}>></script>
    <!--Custom JavaScript -->
    <script src={{('assets/admin/js/ecom-dashboard.js')}}>></script>
    <script src={{ URL::to('assets/js/main.js')}}></script>
