
        <!-- prealoder area start -->
        <div id="loading">
            <div id="loading-center">
                <div id="loading-center-absolute">
                <div class="object" id="first_object"></div>
                <div class="object" id="second_object"></div>
                <div class="object" id="third_object"></div>
                </div>
            </div>
        </div>

     <!-- header area start -->
        <header>
            <div id="header-sticky" class="header__area box-10">
                <div class="container-fluid">
                    <div class="row align-items-center">
                        <div class="col-xl-2 col-lg-2 col-md-4 col-sm-4 col-12">
                            <div class="logo">
                                <a href=""><img src="assets/img/logo/logo.png" alt="logo"></a>
                            </div>
                        </div>
                        <div class="col-xl-5 col-lg-5 col-md-2 col-sm-1 ">
                            <div class="header__right p-relative">
                                <div class="main-menu main-menu-2 d-none d-lg-block ">
                                    <nav>
                                        <ul>
                                        <li class="active"><a href="{{URL::to('index')}}">Home</a>

                                            </li>
                                            <li class="mega-menu has-dropdown"><a href="shop.html">Shop</a>
                                                <ul class="submenu transition-3" data-background="assets/img/bg/mega-menu-bg.jpg">
                                                    <li class="has-dropdown">
                                                        <a href="shop.html">Shop Pages</a>
                                                        <ul>
                                                            <li><a href="shop.html">Standard Shop Page</a></li>
                                                            <li><a href="shop-right-sidebar.html">Shop Right Sidebar</a></li>
                                                            <li><a href="shop-4-col.html">Shop 4 Column</a></li>
                                                            <li><a href="shop-3-col.html">Shop 3 Column</a></li>
                                                            <li><a href="shop.html">Shop Page</a></li>
                                                            <li><a href="shop.html">Shop Page </a></li>
                                                            <li><a href="shop.html">Shop Infinity</a></li>
                                                        </ul>
                                                    </li>
                                                    <li  class="has-dropdown">
                                                        <a href="shop.html">Products Pages</a>
                                                        <ul>
                                                            <li><a href="product-details.html">Product Details</a></li>
                                                            <li><a href="product-details.html">Product Page V2</a></li>
                                                            <li><a href="product-details.html">Product Page V3</a></li>
                                                            <li><a href="product-details.html">Product Page V4</a></li>
                                                            <li><a href="product-details.html">Simple Product</a></li>
                                                            <li><a href="product-details.html">Variable Product</a></li>
                                                            <li><a href="product-details.html">External Product</a></li>
                                                        </ul>
                                                    </li>

                                                </ul>
                                            </li>
                                            <li class="has-dropdown"><a href="">Contact</a>

                                            <ul class="submenu transition-3">
                                                <li class="active"><a href="{{URL::to('about')}}">About Us</a></li>
                                                <li class="active"><a href="{{URL::to('contact')}}">Contact Us</a></li>

                                                </ul>
                                            </li>


                                        </ul>
                                    </nav>
                                </div>

                            </div>
                        </div>
                        <div class="col-xl-5 col-lg-2 col-md-6 col-sm-7">
                            <div class="header__action header__action-2 ">
                                <ul>
                                    <li><a href="#" class="search-toggle"><i class="ion-ios-search-strong"></i> Search</a></li>
                                    <li><a  href="{{URL::to('view-cart')}}" class="cart"><i class="ion-bag"></i> Cart <span></span></a></li>
                                        <li>
                                        <div>
                                            <a href="{{URL::to('login')}}"><button  class="os-btn ">Login</button></a>&nbsp;|
                                            <a href="{{URL::to('register')}}"><button  class="os-btn ">Register</button></a>
                                        </div>
                                        </li>
                                    </li>
                                </ul>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </header>


