@extends('master')

@section('checkout')

<!-- checkout-area start -->
<section class="checkout-area pb-70">
    <div class="container">
    <form action="#">
    <div class="row">
    <div class="col-lg-6">
    <div class="checkbox-form">
    <h3>Billing Details</h3>
    <div class="row">
    <div class="col-md-12">

    </div>
    <div class="col-md-6">
    <div class="checkout-form-list">
    <label>Name <span class="required">*</span></label>
    <input type="text" placeholder=""/>
    </div>
    </div>
    <div class="col-md-6">
        <div class="checkout-form-list">
        <label>Phone <span class="required">*</span></label>
        <input type="text" placeholder="Postcode / Zip" />
        </div>
        </div>

    <div class="col-md-12">
    <div class="checkout-form-list">
    <label>Address <span class="required">*</span></label>
    <input type="text" placeholder="Street address" />
    </div>
    </div>
    <div class="col-md-12">
    <div class="checkout-form-list">
    <input type="text" placeholder="Apartment, suite, unit etc. (optional)" />
    </div>
    </div>
    <div class="col-md-12">
    <div class="checkout-form-list">
    <label>City / Town <span class="required">*</span></label>
    <input type="text" placeholder="City / Town" />
    </div>
    </div>
    <div class="col-md-6">
    <div class="checkout-form-list">
    <label>State / County <span class="required">*</span></label>
    <input type="text" placeholder="" />
    </div>
    </div>
    <div class="col-md-6">
    <div class="checkout-form-list">
    <label>Postcode / Zip <span class="required">*</span></label>
    <input type="text" placeholder="Postcode / Zip" />
    </div>
    </div>

    </div>

    </div>
    </div>
    <div class="col-lg-6">
    <div class="your-order mb-30 ">
    <h3>Your order</h3>
    <div class="your-order-table table-responsive">
    <table>
    <thead>
    <tr>
    <th class="product-name">Product</th>
    <th class="product-total">Total</th>
    </tr>
    </thead>
    <tbody>
    <tr class="cart_item">
    <td class="product-name">
    Vestibulum suscipit <strong class="product-quantity"> × 1</strong>
    </td>
    <td class="product-total">
    <span class="amount">$165.00</span>
    </td>
    </tr>
    <tr class="cart_item">
    <td class="product-name">
    Vestibulum dictum magna <strong class="product-quantity"> × 1</strong>
    </td>
    <td class="product-total">
    <span class="amount">$50.00</span>
    </td>
    </tr>
    </tbody>
    <tfoot>
    <tr class="cart-subtotal">
    <th>Cart Subtotal</th>
    <td><span class="amount">$215.00</span></td>
    </tr>
    <tr class="shipping">
    <th>Shipping</th>
    <td>
    <ul>
    <li>

    <label>
    Flat Rate: <span class="amount">$7.00</span>
    </label>
    </li>
    </ul>
    </td>
    </tr>
    <tr class="order-total">
    <th>Order Total</th>
    <td><strong><span class="amount">$215.00</span></strong>
    </td>
    </tr>
    </tfoot>
    </table>
    </div>

    <div class="payment-method">
    <div class="accordion" id="accordionExample">
    <div class="card">
    <div class="card-header" id="headingOne">
    <h5 class="mb-0">
    <button class="btn-link" type="button" data-toggle="collapse"
    data-target="#collapseOne" aria-expanded="true"
    aria-controls="collapseOne">
    Direct Bank Transfer
    </button>
    </h5>
    </div>

    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne"
    data-parent="#accordionExample">
    <div class="card-body">
    Make your payment directly into our bank account. Please use your Order ID
    as the payment
    reference. Your order won’t be
    shipped until the funds have cleared in our account.
    </div>
    </div>
    </div>
    <div class="card">
    <div class="card-header" id="headingTwo">
    <h5 class="mb-0">
    <button class="btn-link collapsed" type="button" data-toggle="collapse"
    data-target="#collapseTwo" aria-expanded="false"
    aria-controls="collapseTwo">
    Cheque Payment
    </button>
    </h5>
    </div>
    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo"
    data-parent="#accordionExample">
    <div class="card-body">
    Please send your cheque to Store Name, Store Street, Store Town, Store
    State / County, Store
    Postcode.
    </div>
    </div>
    </div>
    <div class="card">
    <div class="card-header" id="headingThree">
    <h5 class="mb-0">
    <button class="btn-link collapsed" type="button" data-toggle="collapse"
    data-target="#collapseThree" aria-expanded="false"
    aria-controls="collapseThree">
    PayPal
    </button>
    </h5>
    </div>
    <div id="collapseThree" class="collapse" aria-labelledby="headingThree"
    data-parent="#accordionExample">
    <div class="card-body">
    Pay via PayPal; you can pay with your credit card if you don’t have a
    PayPal account.
    </div>
    </div>
    </div>
    </div>
    <div class="order-button-payment mt-20">
    <button type="submit" class="os-btn os-btn-black">Place order</button>
    </div>
    </div>
    </div>
    </div>
    </div>
    </form>
    </div>
    </section>
    <!-- checkout-area end --












@endSection
