@extends('master')


@section('user-profile')
<title>Profile</title>

 <main>

        <!-- page title area start -->
        <section class="page__title p-relative d-flex align-items-center" data-background={{ URL::to('assets/img/page-title/page-title-1.jpg')}} style="height:300px;">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="page__title-inner text-center">
                            <h1>Profile</h1>
                            <div class="page__title-breadcrumb">
                                <nav aria-label="breadcrumb">
                                <ol class="breadcrumb justify-content-center">
                                    <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                                    <li class="breadcrumb-item active" aria-current="page"> Account</li>
                                </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- page title area end -->


            {{-- account info here --}}
        <section class="login-area pt-20">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 ">
                        <div class="basic-login">
                            <h3 class="text-center mb-40">Account</h3>
                            <form action="#">
                                <div class="row">
                                    <div class="col-6">

                                    <input id="fname" type="text" placeholder="Enter First Name..." />
                                    </div>
                                    <div class="col-6">

                                        <input id="lname" type="text" placeholder="Enter Last Name..." />
                                    </div>
                                    <div class="col-6">

                                        <input id="address" type="text" placeholder="Enter Address..." />
                                    </div>
                                    <div class="col-6">

                                        <input id="phone" type="text" placeholder="Enter phone #..." />
                                    </div>
                                    <div class="col-6">

                                        <input id="city" type="text" placeholder="Enter City..." />
                                    </div>
                                </div>

                                <center><button class="os-btn col-3 ">Save</button></center>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        {{-- change password  --}}
         <section class="login-area pt-20 mb-40">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="basic-login">
                            <h3 class="text-center mb-40">Change Password</h3>
                            <form action="#">
                                <div class="row">
                                    <div class="col-12">

                                        <input id="currentPassword" type="password" placeholder="Enter Current Password..." />
                                    </div>
                                    <div class="col-6">

                                        <input id="newpassword" type="password" placeholder="Enter New Password..." />
                                    </div>
                                    <div class="col-6">

                                        <input id="confirmPassword" type="password" placeholder="Enter Confirm Password..." />
                                    </div>

                                </div>

                                <center><button class="os-btn col-3">Change Password</button></center>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>


            {{-- change email --}}
        <section class="login-area pt-20 mb-40">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="basic-login">
                            <h3 class="text-center mb-40">Change Email</h3>
                            <form action="#">
                                <div class="row">
                                    <div class="col-6">

                                        <input id="currentEmail" type="email" placeholder="Enter Current Email..." />
                                    </div>
                                    <div class="col-6">
                                        <input id="newEmail" type="email" placeholder="Enter New Email..." />
                                    </div>
                                </div>
                                <center><button class="os-btn col-3">Change Email</button></center>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <center><button class="col-3 btn btn-danger">Delete Profile</button></center>
        <br>
    </main>
@endSection
