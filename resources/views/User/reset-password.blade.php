@extends('master')




@section('resetPassword')

<title>Reset Password</title>

    <main>
        
        <!-- page title area start -->
     
        <section class="page__title p-relative d-flex align-items-center" data-background={{ URL::to('assets/img/bg/background.jpg')}} style="height:300px;">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="page__title-inner text-center">
                            <h1>Reset Password</h1>
                            <div class="page__title-breadcrumb">                                 
                                <nav aria-label="breadcrumb">
                                <ol class="breadcrumb justify-content-center">
                                <li class="breadcrumb-item"><a href="{{URL::to('index')}}}}">Home</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Reset Password</li>
                                </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- page title area end -->

        <!-- reset password Area Strat-->
           <center>
        <section class="login-area pt-100 pb-100 col-8">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 offset-lg-2">
                        <div class="basic-login">
                   
                            <form action="#">
                            
                                <input id="name" type="text" placeholder="Enter Email address..." />
                                
                                <button class="os-btn w-100">Send Email</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </center>
        <!-- login Area End-->
    </main>



@endSection