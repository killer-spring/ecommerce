@extends('master')


@section('product-details')

<title>Product-details</title>
<main>

            <!-- page title area start -->
            <section class="page__title p-relative d-flex align-items-center" data-background="assets/img/bg/background.jpg" style="height:300px;">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="page__title-inner text-center">
                                <h1>Product Details</h1>
                                <div class="page__title-breadcrumb">
                                    <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb justify-content-center">
                                        <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                                        <li class="breadcrumb-item active" aria-current="page"> Product details</li>
                                    </ol>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Added to test product fetch -->


            </section>
            <!-- page title area end -->


            <!-- shop details area start -->
            @if(isset($product))
            <section class="shop__area pb-65">
                <div class="shop__top grey-bg-6 pt-100 pb-90">
                    <div class="container">
                        <div class="row">
                            <div class="col-xl-6 col-lg-6">
                                <div class="product__modal-box d-flex">
                                    <div class="product__modal-nav mr-20">
                                        <nav>
                                            <div class="nav nav-tabs" id="product-details" role="tablist">
                                                <a class="nav-item nav-link active" id="pro-one-tab" data-toggle="tab" href="#pro-one" role="tab" aria-controls="pro-one" aria-selected="true">
                                                <div class="product__nav-img w-img">
                                                    <img src="assets/img/shop/product/details/details-sm-1.jpg" alt="">
                                                </div>
                                                </a>
                                                <a class="nav-item nav-link" id="pro-two-tab" data-toggle="tab" href="#pro-two" role="tab" aria-controls="pro-two" aria-selected="false">
                                                <div class="product__nav-img w-img">
                                                    <img src="assets/img/shop/product/quick-view/quick-sm-2.jpg" alt="">
                                                </div>
                                                </a>
                                                <a class="nav-item nav-link" id="pro-three-tab" data-toggle="tab" href="#pro-three" role="tab" aria-controls="pro-three" aria-selected="false">
                                                <div class="product__nav-img w-img">
                                                    <img src="assets/img/shop/product/quick-view/quick-sm-3.jpg" alt="">
                                                </div>
                                                </a>
                                            </div>
                                        </nav>
                                    </div>
                                    <div class="tab-content mb-20" id="product-detailsContent">
                                        <div class="tab-pane fade show active" id="pro-one" role="tabpanel" aria-labelledby="pro-one-tab">
                                            <div class="product__modal-img product__thumb w-img">
                                                <img src="assets/img/shop/product/details/details-big-1.jpg" alt="">
                                                <div class="product__sale ">
                                                    <span class="new">new</span>
                                                    <span class="percent">-16%</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="pro-two" role="tabpanel" aria-labelledby="pro-two-tab">
                                            <div class="product__modal-img product__thumb w-img">
                                                <img src="assets/img/shop/product/details/details-big-2.jpg" alt="">
                                                <div class="product__sale ">
                                                    <span class="new">new</span>
                                                    <span class="percent">-16%</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="pro-three" role="tabpanel" aria-labelledby="pro-three-tab">
                                            <div class="product__modal-img product__thumb w-img">
                                                <img src="assets/img/shop/product/details/details-big-3.jpg" alt="">
                                                <div class="product__sale ">
                                                    <span class="new">new</span>
                                                    <span class="percent">-16%</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-6 col-lg-6">
                                <div class="product__modal-content product__modal-content-2">
                                    <h4><a href=""></a></h4>

                                    </div>
                                    <div class="product__price-2 mb-25">
                                        <span></span>
                                        <label class="black-color font-weight-bold font-20">Price</label>
                                        <span class="new-price">${{$product['unit_price']}}</span>
                                    </div>
                                     <label class="black-color">Title</label>
                                    <div class="product__modal-des mb-30 black-color">
                                        {{ $product['title'] }}
                                    </div>
                                    <div class="product__modal-form mb-30">
                                        <form action="#">
                                            <div class="product__modal-input size mb-20">
                                                <label>Size <i class="fas fa-star-of-life"></i></label>
                                                <select>
                                                    <option>- Please select -</option>
                                                    <option value="1"> S</option>
                                                    <option value="2"> M</option>
                                                    <option value="3"> L</option>
                                                    <option value="4"> XL</option>
                                                    <option value="5"> XXL</option>
                                                </select>
                                            </div>
                                            <div class="product__modal-input color mb-20">
                                                <label>Color <i class="fas fa-star-of-life"></i></label>
                                                <select>
                                                    <option value=>- Please select -</option>
                                                    <option value="1"> Black</option>
                                                    <option value="2"> Yellow</option>
                                                    <option value="3"> Blue</option>
                                                    <option value="4"> White</option>
                                                    <option value="5"> Ocean Blue</option>
                                                </select>
                                            </div>
                                            <div class="product__modal-required mb-5">
                                                <span >Repuired Fields *</span>
                                            </div>
                                            <div class="pro-quan-area d-sm-flex align-items-center">
                                                <div class="product-quantity-title">
                                                    <label>Quantity</label>
                                                </div>
                                                <div class="product-quantity mr-20 mb-20">
                                                    <div class="cart-plus-minus"><input type="text" name="quantity" value="1" /></div>
                                                </div>

                                            </div>
                                            <br>
                                                <div class="pro-cart-btn">
                                                    <a href="#" class="add-cart-btn mb-20">+ Buy Now</a>
                                                    <a href="" class="add-cart-btn mb-20">+ Add to Cart</a>
                                                </div>
                                        </form>
                                    </div>
                                    {{-- <div class="product__tag mb-25">
                                        <span>Category:</span>
                                        <span><a href="#">Accessories,</a></span>
                                        <span><a href="#">Gaming,</a></span>
                                        <span><a href="#">PC Computers,</a></span>
                                        <span><a href="#">Ultrabooks</a></span>
                                    </div> --}}
                                    {{-- <div class="product__share">
                                        <span>Share :</span>
                                        <ul>
                                            <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                            <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                            <li><a href="#"><i class="fab fa-behance"></i></a></li>
                                            <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                                            <li><a href="#"><i class="fab fa-youtube"></i></a></li>
                                        </ul>
                                    </div> --}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="shop__bottom">
                    <div class="container">
                        <div class="row">
                            <div class="col-xl-12">
                                <div class="product__details-tab">
                                    <div class="product__details-tab-nav text-center mb-45">
                                        <nav>
                                            <div class="nav nav-tabs justify-content-start justify-content-sm-center" id="pro-details" role="tablist">
                                                <a class="nav-item nav-link active" id="des-tab" data-toggle="tab" href="#des" role="tab" aria-controls="des" aria-selected="true">Description</a>
                                                {{-- <a class="nav-item nav-link" id="add-tab" data-toggle="tab" href="#add" role="tab" aria-controls="add" aria-selected="false">Additional Information</a> --}}

                                            </div>
                                        </nav>
                                        <div>
                                            {{ $product['description'] }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            @endif
            <!-- shop details area end -->

            <!-- related products area start -->
            {{-- <section class="related__product pb-60">
                <div class="container">


                    <div class="row">
                        <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
                            <div class="product__wrapper mb-60">
                                <div class="product__thumb">
                                    <a href="product-details.html" class="w-img">
                                        <img src=" " alt="product-img">
                                        <img class="product__thumb-2" src="assets/img/shop/product/product-10.jpg" alt="product-img">
                                    </a>
                                    <div class="product__action transition-3">
                                        <a href="#" data-toggle="tooltip" data-placement="top" title="Add to Wishlist">
                                            <i class="fal fa-heart"></i>
                                        </a>
                                        <a href="#" data-toggle="tooltip" data-placement="top" title="Compare">
                                            <i class="fal fa-sliders-h"></i>
                                        </a>
                                        <!-- Button trigger modal -->
                                        <a href="javascript:void(0);"   data-toggle="modal" data-target="#productModalId">
                                            <i class="fal fa-search"></i>
                                        </a>

                                    </div>
                                    <div class="product__sale">
                                        <span class="new">new</span>
                                        <span class="percent">-16%</span>
                                    </div>
                                </div>
                                <div class="product__content p-relative">
                                    <div class="product__content-inner">
                                        <h4><a href="product-details.html"></a></h4>
                                        <div class="product__price transition-3">

                                            <span class="old-price">$96.00</span>
                                        </div>
                                    </div>
                                    <div class="add-cart p-absolute transition-3">
                                        <a href="#">+ Add to Cart</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            </section> --}}

        </main>






@endSection
