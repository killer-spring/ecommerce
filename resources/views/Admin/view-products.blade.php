
<div id="main-wrapper">
     @include('includes.admin-sidebar')
<!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                    <h2 style="margin-bottom:25px;"> <b>View Products </b>  </h2>
                <!-- ============================================================== -->
                <!-- Info box Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <!-- Column -->
                    @foreach($allProduct as $allProduct)
                    <div class="col-4">



                        <div class="card">


                            <div class="card-body">
                                <div class="product-img">
                                    <img src="{{ $allProduct['image'] }}">
                                    <div class="pro-img-overlay">
                                        <a href="{{ url('/edit-product', $allProduct) }}" style="background-color:#03A9F3" class="fa fa-pencil-square" ></a>
                                        <a href="{{url('/deleteproduct', ['id' => $allProduct->id])}}" style="background-color:#e3483d" class="fa fa-trash" ></a>
                                    </div>
                                </div>
                                <div class="product-text">


                                    <span class="pro-price bg-primary">{{$allProduct['unit_price']}}</span>
                                     <h5 class="card-title m-b-0">{{$allProduct['title']}}</h5>
                                    <small class="text-muted db">{{$allProduct['description']}}</small><br>
                                    <small class="text-muted db">Quantity:{{$allProduct['quantity']}}</small>

                                </div>
                            </div>

                        </div>

                    </div>
                    @endforeach
                 </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>

        </div>

    </div>
</div>
