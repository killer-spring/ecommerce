
<div id="main-wrapper">
    @include('includes.admin-sidebar')
<!-- Page wrapper  -->
       <!-- ============================================================== -->
       <div class="page-wrapper">
           <!-- ============================================================== -->
           <!-- Container fluid  -->


           <!-- ============================================================== -->

           <div class="container-fluid col-6" >

               <!-- ============================================================== -->
               <!-- Info box Content -->
               <!-- ============================================================== -->


                <h1>Edit Product</h1>

                <form enctype='multipart/form-data' action="{{url('/update-product', ['id' => $product->id])}}" method="POST">
                    @csrf
                   @include('products.product-form')
            </form>



           </div>

       </div>

   </div>
</div>
