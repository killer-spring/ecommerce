
<div id="main-wrapper">
    @include('includes.admin-sidebar')
<!-- Page wrapper  -->
       <!-- ============================================================== -->
       <div class="page-wrapper">
           <!-- ============================================================== -->
           <!-- Container fluid  -->
           <!-- ============================================================== -->
           <div class="container-fluid col-6" >

               <!-- ============================================================== -->
               <!-- Info box Content -->
               <!-- ============================================================== -->


                <h1>Add Product</h1>

                @if(session('success'))
                <div class="alert alert-success m-3 ">
                    {{ session('success') }}
                </div>
                @endif



                <form action="{{ URL::to('saveproduct') }}" method="POST" enctype="multipart/form-data">
                    @csrf

                    @include('products.product-form')

                </form>



           </div>

       </div>

   </div>
</div>
