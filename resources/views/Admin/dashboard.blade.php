<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Dashboard</title>
    <link rel="stylesheet" href={{ URL::to('assets/css/bootstrap.min.css')}}>
    <link rel="icon"  sizes="16x16" href={{URL::to('assets/img/favicon.png')}}>
    <link href={{URL::to('assets/admin/css/morris.css')}} rel="stylesheet">
    <link href={{URL::to('assets/admin/css/ecommerce.css')}} rel="stylesheet">
    <!-- Custom CSS -->
    <link href={{URL::to('assets/admin/css/style.min.css')}} rel="stylesheet">
    <link rel="stylesheet" href={{ URL::to('assets/css/preloader.css')}}>
</head>
<body>


        <div id="loading">
            <div id="loading-center">
                <div id="loading-center-absolute">
                <div class="object" id="first_object"></div>
                <div class="object" id="second_object"></div>
                <div class="object" id="third_object"></div>
                </div>
            </div>
        </div>

<div id="main-wrapper">
     @include('includes.admin-sidebar')
     @yield('products')
                <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->

        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Info box Content -->
                <!-- ============================================================== -->

                <div class="row">
                    <!-- Column -->
                    <div class="col-lg-3 col-md-6">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">ORDER RECEIVED</h4>
                                <div class="text-right"> <span class="text-muted">Todays Order</span>
                                    <h1 class="font-light"><sup><i class="ti-arrow-up text-success"></i></sup> 12,000</h1>
                                </div>
                                <span class="text-success">20%</span>
                                <div class="progress">
                                    <div class="progress-bar bg-success" role="progressbar" style="width: 20%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="col-lg-3 col-md-6">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">TAX DEDUCATION</h4>
                                <div class="text-right"> <span class="text-muted">Monthly Deduction</span>
                                    <h1 class="font-light"><sup><i class="ti-arrow-up text-primary"></i></sup> $5,000</h1>
                                </div>
                                <span class="text-primary">30%</span>
                                <div class="progress">
                                    <div class="progress-bar bg-primary" role="progressbar" style="width: 30%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="col-lg-3 col-md-6">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">REVENUE STATS</h4>
                                <div class="text-right"> <span class="text-muted">Todays Income</span>
                                    <h1 class="font-light"><sup><i class="ti-arrow-down text-info"></i></sup> $8,000</h1>
                                </div>
                                <span class="text-info">60%</span>
                                <div class="progress">
                                    <div class="progress-bar bg-info" role="progressbar" style="width: 60%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="col-lg-3 col-md-6">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">YEARLY SALES</h4>
                                <div class="text-right"> <span class="text-muted">Yearly  Income</span>
                                    <h1 class="font-light"><sup><i class="ti-arrow-up text-inverse"></i></sup> $12,000</h1>
                                </div>
                                <span class="text-inverse">80%</span>
                                <div class="progress">
                                    <div class="progress-bar bg-inverse" role="progressbar" style="width: 80%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                </div>
                <!-- ============================================================== -->
                <!-- charts -->
                <!-- ============================================================== -->

                    <!-- Column -->
                    <div class=" col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="d-flex m-b-40 align-items-center no-block">
                                    <h5 class="card-title ">PRODUCT SALES</h5>
                                    <div class="ml-auto">
                                        <ul class="list-inline font-12">
                                            <li><i class="fa fa-circle text-cyan"></i> Iphone</li>
                                            <li><i class="fa fa-circle text-primary"></i> IMac</li>
                                        </ul>
                                    </div>
                                </div>
                                <div id="morris-area-chart2" style="height: 400px;"></div>
                            </div>
                        </div>
                    </div>





                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title">Product Overview</h5>
                                <div class="table-responsive m-t-30">
                                    <table class="table product-overview">
                                        <thead>
                                            <tr>
                                                <th>Customer</th>
                                                <th>Order ID</th>
                                                <th>Photo</th>
                                                <th>Product</th>
                                                <th>Quantity</th>
                                                <th>Date</th>
                                                <th>Status</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Steave Jobs</td>
                                                <td>#85457898</td>
                                                <td>
                                                    <img src="assets/admin/gallery/chair.jpg" alt="iMac" width="80">
                                                </td>
                                                <td>Rounded Chair</td>
                                                <td>20</td>
                                                <td>10-7-2017</td>
                                                <td>
                                                    <span class="label label-success">Paid</span>
                                                </td>

                                            </tr>
                                            <tr>
                                                <td>Varun Dhavan</td>
                                                <td>#95457898</td>
                                                <td>
                                                    <img src="assets/admin/gallery/chair2.jpg" alt="iPhone" width="80">
                                                </td>
                                                <td>Wooden Chair</td>
                                                <td>25</td>
                                                <td>09-7-2017</td>
                                                <td>
                                                    <span class="label label-warning">Pending</span>
                                                </td>

                                            </tr>
                                            <tr>
                                                <td>Ritesh Desh</td>
                                                <td>#68457898</td>
                                                <td>
                                                    <img src="assets/admin/gallery/chair3.jpg" alt="apple_watch" width="80">
                                                </td>
                                                <td>Gray Chair</td>
                                                <td>12</td>
                                                <td>08-7-2017</td>
                                                <td>
                                                    <span class="label label-success">Paid</span>
                                                </td>

                                            </tr>
                                            <tr>
                                                <td>Hrithik</td>
                                                <td>#45457898</td>
                                                <td>
                                                    <img src="assets/admin/gallery/chair4.jpg" alt="mac_mouse" width="80">
                                                </td>
                                                <td>Pure Wooden chair</td>
                                                <td>18</td>
                                                <td>02-7-2017</td>
                                                <td>
                                                    <span class="label label-danger">Failed</span>
                                                </td>

                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->


        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
    </div>








    <script src={{('assets/admin/js/jquery-3.2.1.min.js')}}></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src={{('assets/admin/js/popper.min.js')}}></script>
    <script src={{('assets/admin/js/bootstrap.min.js')}}></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src={{('assets/admin/js/perfect-scrollbar.jquery.min.js')}}>></script>
    <!--Wave Effects -->
    <script src={{('assets/admin/js/waves.js')}}>></script>
    <!--Menu sidebar -->
    <script src={{('assets/admin/js/sidebarmenu.js')}}>></script>
    <!--stickey kit -->
    <script src={{('assets/admin/js/sticky-kit.min.js')}}>></script>
    <script src={{('assets/admin/js/jquery.sparkline.min.js')}}>></script>
    <!--Custom JavaScript -->
    <script src={{('assets/admin/js/custom.min.js')}}>></script>
    <script src={{('assets/admin/js/jquery.sparkline.min.js')}}>></script>
    <!-- ============================================================== -->
    <!-- This page plugins -->
    <!-- ============================================================== -->
    <!--morris JavaScript -->
    <script src={{('assets/admin/js/raphael-min.js')}}>></script>
    <script src={{('assets/admin/js/morris.min.js')}}>></script>
    <!--Custom JavaScript -->
    <script src={{('assets/admin/js/ecom-dashboard.js')}}>></script>
    <script src={{ URL::to('assets/js/main.js')}}></script>
</body>
</html>
