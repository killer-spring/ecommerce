
<div id="main-wrapper">
     @include('includes.admin-sidebar')
<!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
              @if(isset($category))
              <div class="modal-body">
                <h2>Update Category</h2>
            <form action="{{url('/update-category', ['id' => $category->id])}}" method="post">@csrf
              <div class="col-6 mt-3">
             <input  type="text" name="name" class="form-control" value="{{$category['name']}}" />
           </div>
             <div class="col-6 mt-3">
              <label>Sub-Category</label><br/>
              <input  type="text" name="sub_cat" class="form-control" value="{{$category['sub_cat']}}">
            </div>
            <div class="modal-footer col-6">
              <a href="{{ URL::to('/allcategory') }}" type="submit" class="btn btn-secondary">Close</a>
            
            <button type="submit" class="btn btn-primary">Save Changes</button>
          </div>
          </form>
          </div>
          @endif
        </div>

    </div>
</div>
