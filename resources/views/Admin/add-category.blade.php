
<div id="main-wrapper">
  @include('includes.admin-sidebar')
  <!-- Page wrapper  -->
  <!-- ============================================================== -->
  <div class="page-wrapper">
   <!-- ============================================================== -->
   <!-- Container fluid  -->
   <!-- ============================================================== -->
   <div class="container-fluid col-6" >

     <!-- ============================================================== -->
     <!-- Info box Content -->
     <!-- ============================================================== -->
     <h1>Add Category</h1>
      <!-- Button trigger modal -->
     <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter">
      Add New Category
    </button>
     <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">New Category</h5>
          </div>
          <div class="modal-body">
            <form action="{{ URL::to('addCategory') }}" method="post">@csrf
             <input  type="text" name="name" class="form-control" />
             <div class="mt-3">
              <label>Sub-Category</label><br/>
              <input  type="text" name="sub_cat" class="form-control">
            </div>
            <div class="modal-footer">
            <a href="{{ URL::to('allcategory') }}" type="submit" class="btn btn-secondary">Close</a>
            <button type="submit" class="btn btn-primary">Save</button>
          </div>
          </form>
          </div>
        </div>
      </div>
    </div>
    <!--  -->
      <hr>
  <h2>All Categories</h2>
   <div class="row">
    <!-- Column -->
    @foreach($allCategory as $allCategory)                   
    <div class="col-4">
      <div class="card">
        <div class="card-body">
            <div class="product-text">
                <span class="pro-price bg-primary">{{$allCategory['id']}}</span>
            <h5 class="card-title m-b-0">{{$allCategory['name']}}</h5>
            <small class="text-muted db">{{$allCategory['sub_cat']}}</small><br>
          </div>
        </div>
      </div>
      <div><a href="{{url('/edit-category', ['id' => $allCategory->id])}}" class="text-inverse p-r-10"  title="" data-original-title="Edit" data-toggle="tooltip"><i class="fa fa-pencil-square" style="color:#03A9F3; font-size:25px;"></i></a>
       <a href="{{url('/delete', ['id' => $allCategory->id])}}" class="text-inverse" title=""data-toggle="tooltip" data-original-title="Delete"><i class="fa fa-trash" style=" font-size:25px;"></i></a>
      </div>
    </div>
    @endforeach
    </div>
<!--  -->
   </div>
 </div>
</div>
