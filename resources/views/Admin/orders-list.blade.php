<div id="main-wrapper">
    @include('includes.admin-sidebar')



    <div class="page-wrapper">
        <div class="container-fluid">
<div class="row">

    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="card-body">
                <h2 class="card-title">Orders List</h2>
                <div class="table-responsive m-t-30">
                    <table class="table product-overview">
                        <thead>
                            <tr>
                                <th>Customer</th>
                                <th>Order ID</th>
                                <th>Photo</th>
                                <th>Product</th>
                                <th>Quantity</th>
                                <th>Date</th>

                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Steave Jobs</td>
                                <td>#85457898</td>
                                <td>
                                    <img src="assets/admin/gallery/chair.jpg" alt="iMac" width="80">
                                </td>
                                <td>Rounded Chair</td>
                                <td>20</td>
                                <td>10-7-2017</td>

                                <td ><a href="javascript:void(0)" class="text-inverse p-r-10" data-toggle="tooltip" title="" data-original-title="Edit"><i class="fa fa-pencil-square" style="color:#03A9F3; font-size:25px;" ></i></a> <a href="javascript:void(0)" class="text-inverse" title=""
                                        data-toggle="tooltip" data-original-title="Delete"><i class="fa fa-trash" style=" font-size:25px;"></i></a></td>
                            </tr>
                            <tr>
                                <td>Varun Dhavan</td>
                                <td>#95457898</td>
                                <td>
                                    <img src="assets/admin/gallery/chair2.jpg" alt="iPhone" width="80">
                                </td>
                                <td>Wooden Chair</td>
                                <td>25</td>
                                <td>09-7-2017</td>

                                <td><a href="javascript:void(0)" class="text-inverse p-r-10" data-toggle="tooltip" title="" data-original-title="Edit"><i class="fa fa-pencil-square" style="color:#03A9F3; font-size:25px;"></i></a> <a href="javascript:void(0)" class="text-inverse" title=""
                                        data-toggle="tooltip" data-original-title="Delete"><i class="fa fa-trash" style=" font-size:25px;"></i></a></td>
                            </tr>
                            <tr>
                                <td>Ritesh Desh</td>
                                <td>#68457898</td>
                                <td>
                                    <img src="assets/admin/gallery/chair3.jpg" alt="apple_watch" width="80">
                                </td>
                                <td>Gray Chair</td>
                                <td>12</td>
                                <td>08-7-2017</td>

                                <td><a href="javascript:void(0)" class="text-inverse p-r-10" data-toggle="tooltip" title="" data-original-title="Edit"><i class="fa fa-pencil-square" style="color:#03A9F3; font-size:25px;"></i></a> <a href="javascript:void(0)" class="text-inverse" title=""
                                        data-toggle="tooltip" data-original-title="Delete"><i class="fa fa-trash" style=" font-size:25px;"></i></a></td>
                            </tr>
                            <tr>
                                <td>Hrithik</td>
                                <td>#45457898</td>
                                <td>
                                    <img src="assets/admin/gallery/chair4.jpg" alt="mac_mouse" width="80">
                                </td>
                                <td>Pure Wooden chair</td>
                                <td>18</td>
                                <td>02-7-2017</td>

                                <td><a href="javascript:void(0)" class="text-inverse p-r-10" data-toggle="tooltip" title="" data-original-title="Edit"><i class="fa fa-pencil-square" style="color:#03A9F3; font-size:25px;"></i></a> <a href="javascript:void(0)" class="text-inverse" title=""
                                        data-toggle="tooltip" data-original-title="Delete"><i class="fa fa-trash" style=" font-size:25px;"></i></a></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
        </div>
    </div>
</div>
