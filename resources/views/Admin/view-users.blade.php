<script type="text/javascript">
    function ConfirmDelete()
    {
      var x = confirm("Are you sure you want to delete?");
      if (x)
          return true;
      else
        return false;
    }
    function Confirmedit()
    {
      var x = confirm("Are you sure you want to change details?");
      if (x)
          return true;
      else
        return false;
    }
    </script>
<div id="main-wrapper">
    @include('includes.admin-sidebar')
    <div class="page-wrapper">
        <div class="container col-10" >


                <center style="margin-bottom:30px;"><h2><b>Registered Users list</b></h2></center>
                <table class="table table-striped table-dark mt-10 w-50">
                    <tr>
                        <td>Id</td>
                        <td> Name</td>
                        <td>Email</td>
                        <td>password</td>
                        <td>Delete</td>
                        <td>Edit</td>
                    </tr>
                    @if(isset($member))
                    
                        @foreach($member as $memberuser)
                        <tr>
                            <td>{{$memberuser['id']}}</td>
                            <td>{{$memberuser['name']}}</td>
                            <td>{{$memberuser['email']}}</td>
                            <td style="max-width:300px; text-justify">{{$memberuser['password']}}</td>
                            <td><a href = 'userdelete/{{$memberuser["id"]}}' class="btn btn-light" onclick="return ConfirmDelete();">Delete</a></td>
                            <td><a href = 'users-edit/{{ $memberuser["id"]}}' class="btn btn-dark" onclick="return Confirmedit();">Edit</a></td>

                        </tr>
                        @endforeach

                    
                    @endif



                </table>
        </div>
    </div>
</div>

