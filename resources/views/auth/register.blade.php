@extends('layouts.app')
@extends('master')


@section('register')
<main >
        
            <!-- page title area start -->
            <section class="page__title p-relative d-flex align-items-center" data-background="assets/img/bg/background.jpg" style="height:300px;">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="page__title-inner text-center">
                                <h1>Register</h1>
                                <div class="page__title-breadcrumb">                                 
                                    <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb justify-content-center">
                                        <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                                        <li class="breadcrumb-item active" aria-current="page"> Register</li>
                                    </ol>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- page title area end -->

           

            {{-- register  panel --}}
        <section class="login-area pt-20  mb-40">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-8 m-auto">
                        <div class="basic-login" >
                   
                            <form method="POST" action="{{ route('register') }}">
                                @csrf
                                <div class="row">
                                    
                                   
                                     <div class="col-6">
                                        
                                        <input id="name" name="name" type="text" placeholder="Enter Name..." class="form-control @error('name') is-invalid @enderror"  value="{{ old('name') }}" required autocomplete="name" autofocus/>
                                        @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                    </div>
                                    
                                    <div class="col-6">
                                        
                                        <input id="email" name="email" type="email" placeholder="Enter Email..."  type="email" class="form-control @error('email') is-invalid @enderror"  value="{{ old('email') }}" required autocomplete="email" />
                                        @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                    </div>
                                  
                                    <div class="col-6">
                                        
                                        <input id="password" name="password" type="password" placeholder="Enter Password..."  class="form-control @error('password') is-invalid @enderror" required autocomplete="new-password" />
                                        @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                    </div>
                                    <div class="col-6">
                                        
                                        <input id="password-confirm" name="password_confirmation" type="password" placeholder="Enter Confirm Password..."  class="form-control" name="password_confirmation" required autocomplete="new-password" />
                                    </div>
                                     
                                     
                                  
                                     
                                </div>
                                
                                <center><button type="submit" class="os-btn col-3">Register</button></center>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        {{-- rigster panel end --}}

        </main>




@endSection

