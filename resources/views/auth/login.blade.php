@extends('layouts.app')
@extends('master')


@section('login')

 <main>

        <!-- page title area start -->
        <section class="page__title p-relative d-flex align-items-center" data-background={{ URL::to('assets/img/bg/background.jpg')}} style="height:300px;">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="page__title-inner text-center">
                            <h1>Login</h1>
                            <div class="page__title-breadcrumb">
                                <nav aria-label="breadcrumb">
                                <ol class="breadcrumb justify-content-center">
                                    <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                                    <li class="breadcrumb-item active" aria-current="page"> Login</li>
                                </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- page title area end -->

        <!-- login Area Strat-->
        <section class="login-area pt-100 pb-100">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 offset-lg-3">
                        <div class="basic-login">

                            <form method="POST" action="{{ route('login') }}">
                                @csrf

                                <label for="name">Email <span>**</span></label>
                                <input id="name" type="email" name="email"  class="form-control @error('email') is-invalid @enderror" placeholder="Email address..." value="{{ old('email') }}" required autocomplete="email" autofocus />

                                 @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                                <label for="pass">Password <span>**</span></label>
                                <input id="password" name="password" type="password" placeholder="Enter password..." class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password"/>

                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                                <div class="login-action mb-20 fix">
                                    <span class="log-rem f-left">
                                        <input id="remember" type="checkbox" />
                                        <label for="remember">Remember me!</label>
                                    </span>
                                    <span class="forgot-login f-right">
                                    <a href="{{URL::to('reset-password')}}">Lost your password?</a>
                                    </span>
                                </div>

                                <center><button type="submit" class="os-btn w-50">Login Now</button>
                                <div class="or-divide"><span>or</span></div>
                                <a href="{{URL::to('register')}}" class="os-btn os-btn-black w-50">Register Now</a>
                                </center>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- login Area End-->
    </main>
@endsection
