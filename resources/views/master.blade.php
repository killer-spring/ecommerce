<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Place favicon.ico in the root directory -->
        <link rel="shortcut icon" type="image/x-icon" href={{ URL::to('assets/img/favicon.png')}}>

		<!-- CSS here -->
        <link rel="stylesheet" href={{ URL::to('assets/css/preloader.css')}}>
        <link rel="stylesheet" href={{ URL::to('assets/css/bootstrap.min.css')}}>
        <link rel="stylesheet" href={{ URL::to('assets/css/slick.css')}}>
        <link rel="stylesheet" href={{ URL::to('assets/css/metisMenu.css')}}>
        <link rel="stylesheet" href={{ URL::to('assets/css/owl.carousel.min.css')}}>
        <link rel="stylesheet" href={{ URL::to('assets/css/animate.min.css')}}>
        <link rel="stylesheet" href={{ URL::to('assets/css/jquery.fancybox.min.css')}}>
        <link rel="stylesheet" href={{ URL::to('assets/css/fontAwesome5Pro.css')}}>
        <link rel="stylesheet" href={{ URL::to('assets/css/ionicons.min.css')}}>
        <link rel="stylesheet" href={{ URL::to('assets/css/default.css')}}>
        <link rel="stylesheet" href={{ URL::to('assets/css/style.css')}}>

</head>
<body>

    @include('includes.header')

    @yield('home')
    @yield('register')
    @yield('login')
    @yield('resetPassword')
    @yield('contact')
    @yield('about')
    @yield('not-found')
    @yield('product-details')
    @yield('user-profile')
    @yield('privacy-policy')
    @yield('terms-conditions')
    @yield('view-cart')
    @yield('credit-card')
    @yield('delivery-information')
    @yield('shipping-policy')
    @yield('checkout')

    @include('includes.footer')




        <!-- JS here -->
        <script src={{ URL::to('assets/js/vendor/modernizr-3.5.0.min.js')}}></script>
        <script src={{ URL::to('assets/js/vendor/jquery-3.5.1.min.js')}}></script>
        <script src={{ URL::to('assets/js/vendor/waypoints.min.js')}}></script>
        <script src={{ URL::to('assets/js/bootstrap.bundle.min.js')}}></script>
        <script src={{ URL::to('assets/js/metisMenu.min.js')}}></script>
        <script src={{ URL::to('assets/js/slick.min.js')}}></script>
        <script src={{ URL::to('assets/js/jquery.fancybox.min.js')}}></script>
        <script src={{ URL::to('assets/js/isotope.pkgd.min.js')}}></script>
        <script src={{ URL::to('assets/js/owl.carousel.min.js')}}></script>
        <script src={{ URL::to('assets/js/jquery-ui-slider-range.js')}}></script>
        <script src={{ URL::to('assets/js/ajax-form.js')}}></script>
        <script src={{ URL::to('assets/js/wow.min.js')}}></script>
        <script src={{ URL::to('assets/js/imagesloaded.pkgd.min.js')}}></script>
        <script src={{ URL::to('assets/js/main.js')}}></script>
</body>
</html>
