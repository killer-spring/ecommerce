<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CartController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\CheckoutController;
use App\Http\Controllers\Auth\LoginController;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('index', function () {
//     return view('home.index');
// });


Route::get('reset-password', function () {
    return view('user.reset-password');
});

// Route::get('register', function () {
//     return view('User.register');
// });

Route::get('contact', function () {
    return view('static.contact');
});
Route::get('about', function () {
    return view('static.about');
});
Route::get('privacy-policy', function () {
    return view('static.privacy-policy');
});
Route::get('terms-conditions', function () {
    return view('static.terms-conditions');
});

Route::get('delivery-information', function () {
    return view('static.delivery-information');
});

Route::get('shipping-policy', function () {
    return view('static.shipping-policy');
});

Route::get('not-found', function () {
    return view('static.not-found');
});
Route::get('product-details', function () {
    return view('user.product-details');
});
Route::get('user-profile', function () {
    return view('user.user-profile');
});
// Route::get('dashboard', function () {
//     return view('admin.dashboard');
// });
Route::get('view-products', function () {
    return view('admin.view-products');
});
// Route::get('add-product', function () {
//     return view('admin.add-product');
// });

Route::get('view-cart', function () {
    return view('user.view-cart');
});
// Route::get('view-users', function () {
//     return view('admin.view-users');
// });
Route::get('orders-list', function () {
    return view('admin.orders-list');
});
Route::get('checkout', function () {
    return view('user.checkout');
});

Auth::Routes();


 Route::get('/home', [HomeController::class, 'index'])->name('home');




Route::group(['middleware'=>['auth','admin']], function()
{
    Route::get('dashboard', function () {
    return view('admin.dashboard');
    });

    Route::get('/add-product', [ProductController::class, 'create'])->name('createForm');

    Route::get("/edit-product/{id}",[ProductController::class,'showDetails'])->name('editProduct');

    Route::post('/update-product/{id}', [ProductController::class, 'updateProduct']);

    Route::post('/saveproduct', [ProductController::class, 'saveProduct'])->name('saveproduct');

    Route::get('/products', [ProductController::class, 'productsListForAdmin'])->name('products');

    Route::get('/deleteproduct/{id}', [ProductController::class, 'deleteProduct'])->name('deleteProduct');
    // return view('home');
});
Route::group(['middleware'=>['auth','customer']], function(){

});
//logOut
Route::get('/logout',[LoginController:: class,'logout'] );

Route::get('card', function () {
    return view('checkout.card');
});

Route::get('/checkout', [CheckoutController::class, 'checkout']);
Route::post('/recived', [CheckoutController::class, 'afterPayment']);


Route::get('/sendMail',[ContactController:: class,'index'] )->name('Mail');
Route::get('/send-mail',[ContactController:: class,'contactMail'] )->name('contact');
//Added to test product fetch
//please Do not remove the given routes
// Route::get("/product-details/{id}",[ProductController::class,'showDetails']);


Route:: get("/product-details/{id}" ,[ProductController:: class,'productDetails']);

//Add to Cart Route
Route::get("/add-to-cart/{id}",[CartController::class,'add'])->name('addToCart')->middleware('auth');
Route::get("/cart",[CartController::class,'cartItem'])->name('cartItem')->middleware('auth');



// Route::post('/saveproduct', [ProductController::class, 'saveProduct'])->name('saveproduct');
// Route::get('/products', [ProductController::class, 'fatchProducts'])->name('products');

// Route::post('/update-product/{id}', [ProductController::class, 'updateProduct']);

// Route::get('/deleteproduct/{id}', [ProductController::class, 'deleteProduct'])->name('deleteProduct');
Route::get('/users', [AdminController::class, 'showUser']);
Route::get('/users-edit/{id}', [AdminController::class, 'userEdit']);
Route::get('/userdelete/{id}', [AdminController::class, 'deleteUser']);
Route::patch('/users-update/{id}', [AdminController::class, 'userUpdate']);
Route::get('category', function () {
    return view('admin.add-category');
});
Route::get('updatecategory', function () {
    return view('admin.edit-cat');
});
//category methods

Route::post('/addCategory', [CategoryController::class, 'addCategory']);
Route::get('/allcategory', [CategoryController::class, 'fetchCategory']);
Route::get('/deletecategory/{id}', [CategoryController::class, 'remove']);
Route::get('/edit-category/{id}', [CategoryController::class, 'show']);
Route::post('/update-category/{id}', [CategoryController::class, 'update']);
Route::get('/productCategory', [CategoryController::class, 'productCategory']);



Route::get('/index' ,[ProductController::class,'productsForPublic']);
